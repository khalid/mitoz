tabfilter = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	selectInput("selectfilter", label = "Select the tool to use : ", selected = "mitoz_filter", choices = list("mitoz_filter" = "mitoz_filter", "null" = "null")),

conditionalPanel(condition = "input.selectfilter == 'mitoz_filter'",box(title = "MitoZ filter", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("filter__mitoz_filter_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		radioButtons("filter__mitoz_filter_clade", label = "Taxa group for MitoZ : ", choices = list("Arthropoda" = "Arthropoda", "Chordata" = "Chordata", "Echinodermata" = "Echinodermata", "Annelida-segmented-worms" = "Annelida-segmented-worms", "Bryozoa" = "Bryozoa", "Mollusca" = "Mollusca", "Nematoda" = "Nematoda", "Nemertea-ribbon-worms" = "Nemertea-ribbon-worms", "Porifera-sponges" = "Porifera-sponges"),  selected = "Arthropoda", width =  "auto"),

	box(title = "File with adapter list (optional)", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		selectInput("filter__mitoz_adapter_list1_select",label = "Select where to find the file", selected = "server", choices = c("On server" = "server", "On your machine" = "local")),
		conditionalPanel(condition = "input.filter__mitoz_adapter_list1_select == 'server'",
				tags$label("File with adapter list (optional)"),
				fluidRow(
					column(4,shinyFilesButton("shinyfiles_filter__mitoz_adapter_list1",label="Please select a file", title="File with adapter list (optional)", multiple=FALSE)),
					column(8,textInput("filter__mitoz_adapter_list1_server",label=NULL,value=""))
				)
			),
		conditionalPanel(condition = "input.filter__mitoz_adapter_list1_select == 'local'",
			fileInput("filter__mitoz_adapter_list1_local",label = "File with adapter list (optional)")
		)
	)
,

		numericInput("filter__mitoz_Ns_number_SE", label = "Number of threads to use", min = 0, max = NA, step = 1, width =  "auto", value = 10),

	box(title = "File 2 with adapter list (optional)", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		selectInput("filter__mitoz_adapter_list2_PE_select",label = "Select where to find the file", selected = "server", choices = c("On server" = "server", "On your machine" = "local")),
		conditionalPanel(condition = "input.filter__mitoz_adapter_list2_PE_select == 'server'",
				tags$label("File 2 with adapter list (optional)"),
				fluidRow(
					column(4,shinyFilesButton("shinyfiles_filter__mitoz_adapter_list2_PE",label="Please select a file", title="File 2 with adapter list (optional)", multiple=FALSE)),
					column(8,textInput("filter__mitoz_adapter_list2_PE_server",label=NULL,value=""))
				)
			),
		conditionalPanel(condition = "input.filter__mitoz_adapter_list2_PE_select == 'local'",
			fileInput("filter__mitoz_adapter_list2_PE_local",label = "File 2 with adapter list (optional)")
		)
	)
,

		checkboxInput("filter__mitoz_remove_duplicates_PE", label = "filter duplications (caused by PCRs After adapter ligation, the directions of read1 & read2 of duplications should be completely identical)", value = TRUE),

		p("MitoZ filter: A toolkit for assembly, annotation, and visualization of animal mitochondrial genomes"),

		p("Website : ",a(href="https://github.com/linzhi2013/MitoZ","https://github.com/linzhi2013/MitoZ",target="_blank")),

		p("Documentation : ",a(href="https://github.com/linzhi2013/MitoZ","https://github.com/linzhi2013/MitoZ",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1093/nar/gkz173","https://doi.org/10.1093/nar/gkz173",target="_blank"))

	)),
conditionalPanel(condition = "input.selectfilter == 'null'",box(title = "null", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		p("null: Skip this step")

	))))


