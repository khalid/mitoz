tabvisualize = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectvisualize", label = "", value="igv_visualize")),box(title = "IGV visualize", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("visualize__igv_visualize_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		p("IGV visualize: Integrative Genomics Viewer. Fast, efficient, scalable visualization tool for genomics data and annotations "),

		p("Website : ",a(href="https://software.broadinstitute.org/software/igv/","https://software.broadinstitute.org/software/igv/",target="_blank")),

		p("Documentation : ",a(href="https://software.broadinstitute.org/software/igv/","https://software.broadinstitute.org/software/igv/",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1038/nbt.1754","https://doi.org/10.1038/nbt.1754",target="_blank"))

	)))


