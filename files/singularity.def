Bootstrap: localimage
From: ../base.sif

%environment
	export PATH=/opt/biotools/bin:$PATH
	export ROOTSYS=/opt/biotools/root
	export LD_LIBRARY_PATH='$LD_LIBRARY_PATH:$ROOTSYS/lib'
	export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=en_US.UTF-8

%labels
	Author YourName
	Version v0.0.1
	build_date 2018 déc. 07

%runscript
echo "This container contains two apps (UI and Snakemake)."
echo "UI is a user interface to set up the workflow and launch it."
echo "Snakemake let you provide your configfile and other parameters to the snakemake command and launch it."
echo "To get help for an app :\nsingularity help --app appName this_container.sif"
echo "To run an app :\nsingularity run --app appName this_container.sif"


%apprun UI
    exec Rscript -e "shiny::runApp('/sagApp/app.R',host='$1',port=$2)"

%apphelp UI
To run the UI app you should bind data and results directories like in the following example.
You must also provide the host address and port where the shiny app will be launched
exemple : singularity run --app UI -B /path/to/data/directory:/Data -B /path/to/store/Results:/Results this_container.sif 127.0.0.1 1234
    

%apprun Snakemake
    configfile=$1
	cores=$2
	shift
	shift
	exec snakemake -s /workflow/Snakefile all --configfile $configfile --cores $cores $@

%apphelp Snakemake
To run the Snakemake app you should bind data and results directories like in the following example.
You must also provide the configfile and the number of cores provided to snakemake command (you can add other parameters after these two)
exemple : singularity run --app Snakemake -B /path/to/data/directory:/Data -B /path/to/store/Results:/Results this_container.sif myconfig.yml 16 otherparams
    

%apprun getConfigfile
    exec cp /workflow/params.total.yml ./params.yml

%apphelp getConfigfile
To run the getConfigfile app you dont need to bind directories. This app will only copy the default parameters file from the container to your local disk.
exemple : singularity run --app getConfigfile this_container.sif


%apprun getSamples
    exec python3 /workflow/get_samples.py $1 $2

%apphelp getSamples
To run the getSamples app you need to bind the data directory. This app will give you the list of samples detected in a given directory and their file suffix.
exemple : singularity run --app getSamples -B /path/to/data/directory:/Data this_container.sif /Data PE


%help
This container contains four apps (UI, Snakemake, getConfigfile and getSamples).
* UI is a user interface to set up the workflow and launch it.
* Snakemake let you provide your configfile and other parameters to the snakemake command and launch it.
* getConfigfile gives you a copy of a default parameters file to fill and use with the Snakemake app.
* getSamples gives you the list of samples detected in a given directory and their file suffix (usefull for filling samples and sample_suffix in parameters file).
To get help for an app :
singularity help --app appName this_container.sif
To run an app :
singularity run --app appName this_container.sif


%files
		./files /workflow
	./sagApp /sagApp


%post
	mkdir /Data
	mkdir /Results
	apt-get update -y

	pip3 install cutadapt==2.3

	apt-get install -y pigz

	pip3 install biopython==1.69

	pip3 install ete3==3.0.0b35

	pip3 install scipy

	apt-get install -y bioperl

	apt-get install -y circos
	cd /opt/biotools && wget http://circos.ca/distribution/circos-current.tgz
	tar -xzf circos-current.tgz
	rm -rf circos-current.tgz
	mv circos* circos_current
	echo 'export PATH="/opt/biotools/circos_current/bin:$PATH"' >>$SINGULARITY_ENVIRONMENT
	sed -i 's/max_points_per_track.*/max_points_per_track = 40000/' /opt/biotools/circos_current/etc/housekeeping.conf

	cd /opt/biotools
	wget https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2
	tar -xvjf bwa-0.7.17.tar.bz2
	cd bwa-0.7.17
	make -j 10
	mv bwa ../bin/
	cd ..
	rm -r bwa-0.7.17 bwa-0.7.17.tar.bz2

	cd /opt/biotools
	wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2
	tar -xvjf samtools-1.9.tar.bz2
	cd samtools-1.9
	./configure && make
	cd ..
	mv samtools-1.9/samtools bin/samtools
	rm -r samtools-1.9 samtools-1.9.tar.bz2

	cd /opt/biotools
	wget -c https://github.com/linzhi2013/MitoZ/raw/master/version_2.4-alpha/release_MitoZ_v2.4-alpha.tar.bz2
	tar -jxvf release_MitoZ_v2.4-alpha.tar.bz2
	rm release_MitoZ_v2.4-alpha.tar.bz2
	sed -i 's/choices=\["Chordata", "Arthropoda"\]/choices=\["Chordata", "Arthropoda", "Echinodermata", "Annelida-segmented-worms", "Bryozoa", "Mollusca", "Nematoda", "Nemertea-ribbon-worms", "Porifera-sponges"\]/' /opt/biotools/release_MitoZ_v2.4-alpha/MitoZ.py
	echo 'export PATH="/opt/biotools/release_MitoZ_v2.4-alpha:$PATH"'' >>$SINGULARITY_ENVIRONMENT
	python3 -c "from ete3 import NCBITaxa;ncbi = NCBITaxa();ncbi.update_taxonomy_database()"

	apt-get install -y ncbi-tools-bin

	apt-get install -y xvfb

	cd /opt/biotools
	wget -c https://data.broadinstitute.org/igv/projects/downloads/2.6/IGV_Linux_2.6.2.zip
	unzip IGV_Linux_2.6.2.zip
	rm IGV_Linux_2.6.2.zip
	echo 'export PATH="/opt/biotools/IGV_Linux_2.6.2:$PATH"' >>$SINGULARITY_ENVIRONMENT
	sed -i 's/-Xmx4g/-Xmx16g/' /opt/biotools/IGV_Linux_2.6.2/igv.sh

mkdir -p /share/apps/bin
mkdir -p /share/apps/lib
mkdir -p /share/apps/gridengine
mkdir -p /share/bio
mkdir -p /opt/gridengine
mkdir -p /export/scrach
mkdir -p /usr/lib64
ln -s /bin/bash /bin/mbb_bash   
ln -s /bin/bash /bin/isem_bash
/usr/sbin/groupadd --system --gid 400 sge
/usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge